BUILD_DIR:=_build/
CC:=gcc
AR:=ar
CFLAGS:=-Wall -Wextra -Werror -g -I.
DOXYGEN:=doxygen

C_SOURCES+=$(wildcard src/*.c)
HEADERS+=$(wildcard *.h)

C_OBJECTS:=$(C_SOURCES:src/%.c=$(BUILD_DIR)/%.o)

all: croutines.a

.PHONY: croutines.a doc clean-doc

croutines.a: $(C_OBJECTS)
	$(AR) rcs $@ $(C_OBJECTS)

$(C_OBJECTS): $(BUILD_DIR)/%.o : src/%.c $(HEADERS) Makefile | $(BUILD_DIR)
	$(CC) -c $(CFLAGS) $< -o $@

doc:
	$(DOXYGEN) doc/Doxyfile

ifeq ($(OS),Windows_NT)
$(BUILD_DIR):
	-powershell "New-Item $(BUILD_DIR) -ItemType Directory -ea 0"
clean:
	-powershell "Remove-Item -Recurse -Force $(BUILD_DIR)"
clean-doc:
	-powershell "Remove-Item -Recurse -Force doc/html"
	-powershell "Remove-Item -Recurse -Force doc/latex"
else
$(BUILD_DIR):
	-mkdir -p $(BUILD_DIR)
clean:
	-rm -rf $(BUILD_DIR)
clean-doc:
	-rm -rf doc/html
	-rm -rf doc/latex
endif

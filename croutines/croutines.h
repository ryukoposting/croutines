#ifndef __CROUTINES_H__
#define __CROUTINES_H__

#include <semaphore.h>
#include <stddef.h>
#include <ucontext.h>

#include "croutines/croutines_config.h"

typedef struct task task_t;
typedef struct looper looper_t;
typedef struct cancel_action cancel_action_t;

typedef void (*task_func_t)(task_t*, void*);

struct task {
  ucontext_t uc;
  looper_t *looper;
  task_t *next;
  task_t *parent;
  cancel_action_t *cancel_actions;
  void *await_result;
  int got_await_result;
  int await_multiple_slot;

  int dynamic : 1;
  int scheduled : 1;
  int canceled : 1;
};

struct cancel_action {
  void (*func)(void *arg);
  void *arg;
  struct cancel_action *next;
};

struct looper {
  size_t default_stack_sz;
  ucontext_t ucl;
  task_t *ready;
#if CROUTINES_THREADS
  task_t *ready_from_other_thread;
  sem_t ready_lock;
#endif
};

#endif /* __CROUTINES_H__ */

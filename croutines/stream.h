#ifndef __CROUTINES_STREAM_H__
#define __CROUTINES_STREAM_H__

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>

#include "croutines/croutines.h"

#define AS_SUCCESS ((void*)0)
#define AS_ERROR ((void*)1)

typedef struct async_ostream async_ostream_t;
typedef struct async_istream async_istream_t;

struct async_ostream {
  task_func_t (*write)(void const *ptr, size_t size, size_t nmemb, async_ostream_t *self);
  task_func_t (*printf)(char const *fmt, async_ostream_t *self, ...);
  union {
    int fd;
    FILE *fp;
  };
};

struct async_istream {
  task_func_t (*read)(void *ptr, size_t size, size_t nmemb, async_istream_t *self);
  union {
    int fd;
    FILE *fp;
  };
};

void async_ostream_init(FILE *fp, async_ostream_t *ostream);
void async_ostream_init_fd(int fd, async_ostream_t *ostream);

void async_istream_init(FILE *fp, async_istream_t *istream);
void async_istream_init_fd(int fd, async_istream_t *istream);

#endif /* __CROUTINES_STREAM_H__ */

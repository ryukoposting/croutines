/**
 * @file async.h
 * @author Evan Perry Grove (epg@tfwno.gf)
 * @version 0.1
 * @date 2021-03-21
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#ifndef __CROUTINES_ASYNC_H__
#define __CROUTINES_ASYNC_H__

#include <sys/time.h>

#include "croutines/croutines.h"

/**
 * @brief Initialize a looper.
 * 
 * @param default_stack_sz 
 * @param looper 
 * @return int 
 */
int init_looper(size_t default_stack_sz, looper_t *looper);
task_t *new_task(task_func_t entry, void *arg, looper_t *looper);
void run_until_complete(task_t *task, looper_t *looper);

void set_result(void *result, task_t *task);

/* Suspend execution of the current task. The task will be resumed later. */
void yield(task_t *task);

/* Call the given coroutine function with argument `arg`, and get its result
  when it completes. */
void *await(task_func_t entry, void *arg, task_t *task);

/* Same as await(), but with a timeout. if `did_timeout` is not null, it will
  be set to 0 or 1 to represent whether the timeout occurred. */
void *await_timeout(task_func_t entry, void *arg, struct timeval const *tv, int *did_timeout, task_t *task);
void *await_multiple(int nawaits, task_t *task, ...);
void *await_multiple_timeout(int nawaits, struct timeval const *tv, int *did_timeout, task_t *task, ...);

void add_on_cancel(cancel_action_t *cancel_action, task_t *task);
void remove_on_cancel(cancel_action_t *cancel_action, task_t *task);

void async_sleep(struct timeval const *tv, task_t *task);

#endif /* __CROUTINES_ASYNC_H__ */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>

#include "croutines/async.h"

/* this message will be passed from tfunc to foo_handler */
struct foo {
  char const *msg;
  unsigned nsecs;
};

static looper_t looper;


void foo_handler(task_t *task, void *arg)
{
  struct foo *p = (struct foo*)arg;
  struct timeval tv = { .tv_sec = p->nsecs };
  async_sleep(&tv, task); /* sleep for `nsecs` seconds */

  char *s = strdup(p->msg);

  /* make sure s gets freed if the task gets
    canceled before tfunc receives the result */
  cancel_action_t ca = { free, s };
  add_on_cancel(&ca, task);

  set_result(s, task);
}

void tfunc(task_t *task, void *arg)
{
  struct foo foos[] = {
    { "thing one", 3 },
    { "thing two", 1 },
    { "thing three", 4 },
  };

  struct timeval tv = { .tv_sec = 6 };
  int did_timeout;
  void **results = await_multiple_timeout(3, &tv, &did_timeout, task,
    foo_handler, &foos[0],
    foo_handler, &foos[1],
    foo_handler, &foos[2]
  );

  if (did_timeout) {
    printf("timeout!\n");
  } else {
    printf("0: %s\n", (char*)results[0]);
    printf("1: %s\n", (char*)results[1]);
    printf("2: %s\n", (char*)results[2]);
  }

  free(results[0]);
  free(results[1]);
  free(results[2]);
  free(results);
}

int main()
{

  init_looper(0, &looper);
  task_t *t = new_task(tfunc, NULL, &looper);
  run_until_complete(t, &looper);

  return 0;
}


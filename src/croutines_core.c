#include <unistd.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include "croutines/async.h"


static int timeval_subtract(struct timeval *result, struct timeval const *x, struct timeval *y);
static int init_task(task_t *task, task_func_t entry, void *arg, looper_t *looper);
static void cancel_task_children(task_t *task);
static void destroy_task(task_t *task);
static void run_soon(task_t *task);
static void run_later(task_t *task);


int init_looper(size_t default_stack_sz, looper_t *looper)
{
  int result = 0;
  memset(looper, 0, sizeof(*looper));
  looper->default_stack_sz = default_stack_sz;
#if CROUTINES_THREADS
  sem_init(&looper->ready_lock, 0, 1);
#endif
  return result;
}

task_t *new_task(task_func_t entry, void *arg, looper_t *looper)
{
  task_t *result = calloc(1, sizeof(*result));

  if (init_task(result, entry, arg, looper)) {
    free(result);
    result = NULL;
  } else {
    result->dynamic = 1;
  }

  return result;
}

void run_until_complete(task_t *task, looper_t *looper)
{
  task->next = looper->ready;
  looper->ready = task;

  while (looper->ready) {
    task_t *t = looper->ready;
    looper->ready = looper->ready->next;
    t->scheduled = 0;
    t->next = NULL;

    if (t->canceled) {
      for (cancel_action_t *ca = t->cancel_actions; ca; ca = ca->next)
        ca->func(ca->arg);
    } else {
      swapcontext(&looper->ucl, &t->uc);
    }

    if (!t->scheduled) {
      destroy_task(t);
    }
  }
}

void set_result(void *result, task_t *task)
{
  if (task->parent) {
    if (task->await_multiple_slot) {
      ((void**)task->parent->await_result)[task->await_multiple_slot - 1] = result;
    } else {
      task->parent->await_result = result;
    }
    task->parent->got_await_result += 1;
  }
}

void yield(task_t *task)
{
  run_later(task);
  swapcontext(&task->uc, &task->looper->ucl);
}

void *await(task_func_t entry, void *arg, task_t *task)
{
  task_t *t = new_task(entry, arg, task->looper);
  t->parent = task;
  run_later(t);
  
  while (!task->got_await_result) {
    yield(task);
  }

  cancel_task_children(task);

  task->got_await_result = 0;

  return task->await_result;
}

void *await_timeout(task_func_t entry, void *arg, struct timeval const *tv, int *did_timeout, task_t *task)
{
  task_t *t = new_task(entry, arg, task->looper);
  t->parent = task;
  run_later(t);

  struct timeval tv1;
  gettimeofday(&tv1, NULL);
  tv1.tv_sec += tv->tv_sec;
  tv1.tv_usec += tv->tv_usec;
  if (tv1.tv_usec >= 1000000) {
    tv1.tv_usec -= 1000000;
    tv1.tv_sec += 1;
  }

  if (did_timeout)
    *did_timeout = 0;

  while (!task->got_await_result) {
    yield(task);
    
    struct timeval tv2, result;
    gettimeofday(&tv2, NULL);
  
    if (timeval_subtract(&result, &tv1, &tv2)) {
      if (did_timeout)
        *did_timeout = !task->got_await_result;
      break;
    }
  }

  cancel_task_children(task);

  task->got_await_result = 0;

  return task->await_result;
}

void *await_multiple(int nawaits, task_t *task, ...)
{
  va_list lst;
  task->await_result = calloc(nawaits, sizeof(void*));

  va_start(lst, task);
  for (int i = 0; i < nawaits; ++i) {
    task_func_t entry = va_arg(lst, task_func_t);
    void *arg = va_arg(lst, void*);
    task_t *t = new_task(entry, arg, task->looper);
    t->parent = task;
    t->await_multiple_slot = i + 1;
    run_later(t);
  }
  va_end(lst);

  while (task->got_await_result < nawaits) {
    yield(task);
  }

  cancel_task_children(task);

  task->got_await_result = 0;

  return task->await_result;
}

void *await_multiple_timeout(int nawaits, struct timeval const *tv, int *did_timeout, task_t *task, ...)
{
  va_list lst;
  task->await_result = calloc(nawaits, sizeof(void*));

  va_start(lst, task);
  for (int i = 0; i < nawaits; ++i) {
    task_func_t entry = va_arg(lst, task_func_t);
    void *arg = va_arg(lst, void*);
    task_t *t = new_task(entry, arg, task->looper);
    t->parent = task;
    t->await_multiple_slot = i + 1;
    run_later(t);
  }
  va_end(lst);

  struct timeval tv1;
  gettimeofday(&tv1, NULL);
  tv1.tv_sec += tv->tv_sec;
  tv1.tv_usec += tv->tv_usec;
  if (tv1.tv_usec >= 1000000) {
    tv1.tv_usec -= 1000000;
    tv1.tv_sec += 1;
  }

  if (did_timeout)
    *did_timeout = 0;

  while (task->got_await_result < nawaits) {
    yield(task);
    
    struct timeval tv2, result;
    gettimeofday(&tv2, NULL);
  
    if (timeval_subtract(&result, &tv1, &tv2)) {
      if (did_timeout)
        *did_timeout = task->got_await_result < nawaits;
      break;
    }
  }

  cancel_task_children(task);

  task->got_await_result = 0;

  return task->await_result;
}

void async_sleep(struct timeval const *tv, task_t *task)
{
  struct timeval tv1, tv2, result;
  gettimeofday(&tv1, NULL);
  tv1.tv_sec += tv->tv_sec;
  tv1.tv_usec += tv->tv_usec;
  if (tv1.tv_usec >= 1000000) {
    tv1.tv_usec -= 1000000;
    tv1.tv_sec += 1;
  }

  do {
    yield(task);
    gettimeofday(&tv2, NULL);
  } while (!timeval_subtract(&result, &tv1, &tv2));
}

void add_on_cancel(cancel_action_t *cancel_action, task_t *task)
{
  if (!cancel_action->next) {
    cancel_action->next = task->cancel_actions;
    task->cancel_actions = cancel_action;
  }
}

void remove_on_cancel(cancel_action_t *cancel_action, task_t *task)
{
  cancel_action_t *pp = NULL, *p = task->cancel_actions;
  while (p) {
    if (p == cancel_action) {
      if (!pp) {
        task->cancel_actions = p->next;
      } else {
        pp->next = p->next;
      }
      cancel_action->next = NULL;
      p = p->next;
    } else {
      pp = p;
      p = p->next;
    }
  }
}


static int timeval_subtract(struct timeval *result, struct timeval const *x, struct timeval *y)
{ /* y - x */
  if (x->tv_usec < y->tv_usec) {
    int nsec = (y->tv_usec - x->tv_usec) / 1000000 + 1;
    y->tv_usec -= 1000000 * nsec;
    y->tv_sec += nsec;
  }
  if (x->tv_usec - y->tv_usec > 1000000) {
    int nsec = (x->tv_usec - y->tv_usec) / 1000000;
    y->tv_usec += 1000000 * nsec;
    y->tv_sec -= nsec;
  }

  result->tv_sec = x->tv_sec - y->tv_sec;
  result->tv_usec = x->tv_usec - y->tv_usec;

  return (x->tv_sec < y->tv_sec) || ((x->tv_sec == y->tv_sec) && (x->tv_usec < y->tv_usec));
}

static int init_task(task_t *task, task_func_t entry, void *arg, looper_t *looper)
{
  int result;

  memset(task, 0, sizeof(*task));

  result = getcontext(&task->uc);
  if (result)
    return result;

  task->uc.uc_stack.ss_size = looper->default_stack_sz 
    ? looper->default_stack_sz
    : CROUTINES_TASK_STACK_SZ;
  task->uc.uc_link = &looper->ucl;
  task->uc.uc_stack.ss_sp = malloc(task->uc.uc_stack.ss_size);
  if (!task->uc.uc_stack.ss_sp)
    return -1;
  
  task->looper = looper;

  makecontext(&task->uc, (void(*)()) entry, 2, task, arg);

  return result;
}

static void cancel_task_children(task_t *task)
{
  looper_t *looper = task->looper;

  task_t *p = looper->ready;

  while (p) {
    if (p->parent == task) {
      p->canceled = 1;
      cancel_task_children(p);
    }
    p = p->next;
  }
}

static void destroy_task(task_t *task)
{
  free(task->uc.uc_stack.ss_sp);
  if (task->dynamic) {
    free(task);
  }
}

static void run_soon(task_t *task)
{
  task->next = task->looper->ready;
  task->looper->ready = task;
  task->scheduled = 1;
}

static void run_later(task_t *task)
{
  if (!task->looper->ready)
    run_soon(task);
  else {
    task_t *p = task->looper->ready;
    while (p->next)
      p = p->next;
    p->next = task;
    task->next = NULL;
  }
  task->scheduled = 1;
}